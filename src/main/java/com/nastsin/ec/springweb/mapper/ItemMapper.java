package com.nastsin.ec.springweb.mapper;

import com.nastsin.ec.springweb.dto.ItemDto;
import com.nastsin.ec.springweb.entity.Item;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ItemMapper {
    private final ModelMapper mapper;

    @Autowired
    public ItemMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public Item toEntity(ItemDto addressDto) {
        Item item =  Objects.isNull(addressDto) ? null : mapper.map(addressDto, Item.class);
        if(item != null){
            item.getUser().getItems().add(item);
        }
        return item;
    }

    public ItemDto toDto(Item entity) {
        return Objects.isNull(entity) ? null : mapper.map(entity, ItemDto.class);
    }
}
